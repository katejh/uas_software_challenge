#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include <thread>
#include <chrono>

#define PI 3.141592653
#define WAYPOINTS_FILE "missionwaypoints.txt"
#define EQUATORIAL_RADIUS 6378.137 //km
#define INVERSE_FLATTENING 298.257223563

// function prototype declarations
void convertLatLongToUTM(double, double, double*, double*);
double convertDegreeToRadian(double);
double getDistance(double, double, double, double);

int main() {
    std::ifstream inFile;
    double latitude, longitude;
    double easting, northing, eastingPrev, northingPrev;
    double distance; // for holding the current distance travelled, and for calculating instantaneous speed
    double totalDistance = 0.0, totalTime = 0.0; // used for calculating the average speed overall
    double speed, averageSpeed;
    
    // let the terminal print numbers without scientific notation
    std::cout << std::fixed;

    // open the waypoints file for reading
    inFile.open(WAYPOINTS_FILE);

    // check that the waypoints file has been opened
    if (!inFile.is_open()) {
        std::cerr << "Error! Unable to open file " << WAYPOINTS_FILE << std::endl;
        exit(1);
    }

    // read first set of waypoints
    inFile >> latitude >> longitude;
    convertLatLongToUTM(latitude, longitude, &eastingPrev, &northingPrev);
    
    while (inFile >> latitude >> longitude) {
        convertLatLongToUTM(latitude, longitude, &easting, &northing); 
        
        // calculate current distance
        distance = getDistance(easting, northing, eastingPrev, northingPrev);
        totalDistance += distance;
        totalTime = totalTime + 1.0;

        // current instantaneous speed is simply the distance travelled in the last second
        speed = distance;
        std::cout << "Current speed: " << speed << " m/s\tTime passed: " << totalTime << " seconds" << std::endl;

        eastingPrev = easting;
        northingPrev = northing;

        // sleep for one second - current speed updates every second since waypoints are one second apart
        std::this_thread::sleep_for(std::chrono::seconds(1));     
    }

    // print average speed overall
    averageSpeed = totalDistance / totalTime;
    std::cout << "Average speed: " << averageSpeed << " m/s" << std::endl;

    return 0;    
}

/*
 * Converts GPS latitude and longitude coordinates (in degrees) to UTM easting and northing coordinates (in metres)
 * formula from https://en.wikipedia.org/wiki/Universal_Transverse_Mercator_coordinate_system#Locating_a_position_using_UTM_coordinates
 * 
 * @param lat the GPS latitude coordinate
 * @param lon the GPS longitude coordinate
 * @param eas pointer variable for the easting coordinate to be assigned
 * @param nor pointer variable for the northing coordinate to be assigned
 * 
 * @return void
 */
void convertLatLongToUTM(double lat, double lon, double *eas, double *nor) {
    double a, f, n, A;
    double alph_1, alph_2, alph_3;
    double beta_1, beta_2, beta_3;
    double delt_1, delt_2, delt_3;
    double t, xi_prime, eta_prime;
    double E, N;
    // reference meridian
    // in BC, UTM zone is 10 where central meridian is -123 degrees
    const double lon_0 = -123; // degrees
    // by convention, N_0 is 0 km in northern hemisphere and 10000km in southern hemisphere. BC is in northern hemisphere
    const double N_0 = 0.0; //km
    const double k_0 = 0.9996;
    const double E_0 = 500; //km

    // compute preliminary values
    a = EQUATORIAL_RADIUS;
    f = 1 / INVERSE_FLATTENING;
    n = f / (2 - f);
    A = (a / (1 + n)) * (1 + (pow(n, 2.0) / 4) + (pow(n, 4.0) / 64) + (pow(n, 6.0) / 256));

    alph_1 = ((float)1/2) * n - ((float)2/3) * pow(n, 2.0) + ((float)5/16) * pow(n, 3.0);
    alph_2 = ((float)13/48) * pow(n, 2.0) - ((float)3/5) * pow(n, 3.0);
    alph_3 = ((float)61/420) * pow(n, 3.0);

    beta_1 = ((float)1/2) * n - ((float)2/3) * pow(n, 2.0) + ((float)37/96) * pow(n, 3.0);
    beta_2 = ((float)1/48) * pow(n, 2.0) + ((float)1/15) * pow(n, 3.0);
    beta_3 = ((float)17/480) * pow(n, 3.0);

    delt_1 = 2 * n - ((float)2/3) * pow(n, 2.0) - 2 * pow(n, 3.0);
    delt_2 = ((float)7/3) * pow(n, 2.0) - ((float)8/5) * pow(n, 3.0);
    delt_3 = ((float)56/15) * pow(n, 3.0);

    // compute intermediate values
    t = sinh(atanh(sin(convertDegreeToRadian(lat))) - ((2 * sqrt(n)) / (1 + n)) * atanh(((2 * sqrt(n)) / (1 + n)) * sin(convertDegreeToRadian(lat))));
    xi_prime = atan(t / cos(convertDegreeToRadian(lon - lon_0)));
    eta_prime = atanh(sin(convertDegreeToRadian(lon - lon_0)) / sqrt(1 + pow(t, 2.0)));

    // compute easting
    E = E_0 + k_0 * A * (eta_prime + (alph_1 * cos(2 * 1 * xi_prime) * sinh(2 * 1 * eta_prime)) + (alph_2 * cos(2 * 2 * xi_prime) * sinh(2 * 2 * eta_prime)) + (alph_3 * cos(2 * 3 * xi_prime) * sinh(2 * 3 * eta_prime)));
    // compute northing
    N = N_0 + k_0 * A * (xi_prime + (alph_1 * sin(2 * 1 * xi_prime) * cosh(2 * 1 * eta_prime)) + (alph_2 * sin(2 * 2 * xi_prime) * cosh(2 * 2 * eta_prime)) + (alph_3 * sin(2 * 3 * xi_prime) * cosh(2 * 3 * eta_prime)));

    // return easting and northing in metres
    *eas = E * 1000;
    *nor = N * 1000;
}

/*
 * Converts degree to radian 
 * 
 * @param angle angle in degrees
 * 
 * @return angle in radians
 */
double convertDegreeToRadian(double angle) {
    return (angle * PI) / 180.0;
}

/*
 * Finds distance between two points/coordinates
 * 
 * @param x1 x-coordinate of first point
 * @param y1 y-coordinate of first point
 * @param x2 x-coordinate of second point
 * @param y2 y-coordinate of second point
 * 
 * @return the distance between the two points
 */
double getDistance(double x1, double y1, double x2, double y2) {
    return sqrt(pow((x2 - x1), 2.0) + pow((y2 - y1), 2.0));
}