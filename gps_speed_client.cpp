
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>

#define WAYPOINTS_FILE "missionwaypoints.txt"

// some code from https://www.geeksforgeeks.org/socket-programming-cc/
int main(int argc, char* argv[]) {
  std::ifstream inFile;
  int sock_fd, port;
  struct sockaddr_in serv_addr;

  // open the waypoints file for reading
  inFile.open(WAYPOINTS_FILE);

  // open socket
  if ((sock_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    std::cerr << "Error: could not connect open socket" << std::endl;
    exit(1);

  // check if port number is specified, otherwise set it to local port 8080
  if (argc == 2) {
    port = atoi(argv[1]);
  }
  else {
    port = 8080;
  }

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(port);

  if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
      printf("\nInvalid address/ Address not supported \n");
      return -1;
  }

  if (connect(sock_fd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)  {
      std::cerr << "Error connecting" << std::endl;
      exit(1);
  }

  send(sock_fd, &inFile, sizeof(inFile), 0);



  return 0;
}

