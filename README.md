# UAS Software Challenge

## Functionality
* Can take a file containing GPS coordinates and convert it to UTM coordinates in metres
* Outputs the current speed once per second
* Outputs the overall average speed at the end

#### Not functional 
* Serving of server and client not functioning
* I took on this challenge because I wanted to challenge myself, as I was new to writing servers and clients! This was my first time dealing with server and client concepts, and unfortunately, the server and client I've written does not seem to work, and I couldn't figure it out in time. 
* I have added a separate standalone file that runs without the server/client stuff that otherwise fulfills the challenge requirements